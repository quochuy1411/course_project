var gCoursesDB = {
  description: "This DB includes all courses in system",
  courses: [
    {
      id: 1,
      courseCode: "FE_WEB_ANGULAR_101",
      courseName: "How to easily create a website with Angular",
      price: 750,
      discountPrice: 600,
      duration: "3h 56m",
      level: "Beginner",
      coverImage: "images/courses/course-angular.jpg",
      teacherName: "Morris Mccoy",
      teacherPhoto: "images/teacher/morris_mccoy.jpg",
      isPopular: false,
      isTrending: true,
    },
    {
      id: 2,
      courseCode: "BE_WEB_PYTHON_301",
      courseName: "The Python Course: build web application",
      price: 1050,
      discountPrice: 900,
      duration: "4h 30m",
      level: "Advanced",
      coverImage: "images/courses/course-python.jpg",
      teacherName: "Claire Robertson",
      teacherPhoto: "images/teacher/claire_robertson.jpg",
      isPopular: false,
      isTrending: true,
    },
    {
      id: 5,
      courseCode: "FE_WEB_GRAPHQL_104",
      courseName: "GraphQL: introduction to graphQL for beginners",
      price: 850,
      discountPrice: 650,
      duration: "2h 15m",
      level: "Intermediate",
      coverImage: "images/courses/course-graphql.jpg",
      teacherName: "Ted Hawkins",
      teacherPhoto: "images/teacher/ted_hawkins.jpg",
      isPopular: true,
      isTrending: false,
    },
    {
      id: 6,
      courseCode: "FE_WEB_JS_210",
      courseName: "Getting Started with JavaScript",
      price: 550,
      discountPrice: 300,
      duration: "3h 34m",
      level: "Beginner",
      coverImage: "images/courses/course-javascript.jpg",
      teacherName: "Ted Hawkins",
      teacherPhoto: "images/teacher/ted_hawkins.jpg",
      isPopular: true,
      isTrending: true,
    },
    {
      id: 8,
      courseCode: "FE_WEB_CSS_111",
      courseName: "CSS: ultimate CSS course from beginner to advanced",
      price: 750,
      discountPrice: 600,
      duration: "3h 56m",
      level: "Beginner",
      coverImage: "images/courses/course-css.jpg",
      teacherName: "Juanita Bell",
      teacherPhoto: "images/teacher/juanita_bell.jpg",
      isPopular: true,
      isTrending: true,
    },
    {
      id: 14,
      courseCode: "FE_WEB_WORDPRESS_111",
      courseName: "Complete Wordpress themes & plugins",
      price: 1050,
      discountPrice: 900,
      duration: "4h 30m",
      level: "Advanced",
      coverImage: "images/courses/course-wordpress.jpg",
      teacherName: "Clevaio Simon",
      teacherPhoto: "images/teacher/clevaio_simon.jpg",
      isPopular: true,
      isTrending: false,
    },
  ],
};
("use strict");
// Biến toàn cục để lưu trữ id COURSES đang đc update or delete. Mặc định = 0;
var gCourse = 0;
// Biến mảng hằng số chứa danh sách tên các thuộc tính
const gCOURSES_COLS = [
    "id",
    "courseCode",
    "courseName",
    "price",
    "discountPrice",
    "duration",
    "level",
    "coverImage",
    "teacherName",
    "teacherPhoto",
    "isPopular",
    "isTrending",
    "action",
];

// Biến mảng toàn cục định nghĩa chỉ số các cột tương ứng
const gCOURSES_ID_COL = 0;
const gCOURSES_COURSECODE_COL = 1;
const gCOURSES_COURSENAME_COL = 2;
const gCOURSES_PRICE_COL = 3;
const gCOURSES_DISCOUNTPRICE_COL = 4;
const gCOURSES_DURATION_COL = 5;
const gCOURSES_LEVEL_COL = 6;
const gCOURSES_COVERIMAGE_COL = 7
const gCOURSES_TEACHERNAME_COL = 8;
const gCOURSES_TEACHERPHOTO_COL = 9;
const gCOURSES_ISPOPULAR_COL = 10;
const gCOURSES_TRENDING_COL = 11;
const gCOURSES_ACTION_COL = 12;
var gCoursesTable = $("#courses-table").DataTable({
    columns: [
        { data: gCOURSES_COLS[gCOURSES_ID_COL] },
        { data: gCOURSES_COLS[gCOURSES_COURSECODE_COL] },
        { data: gCOURSES_COLS[gCOURSES_COURSENAME_COL] },
        { data: gCOURSES_COLS[gCOURSES_PRICE_COL] },
        { data: gCOURSES_COLS[gCOURSES_DISCOUNTPRICE_COL] },
        { data: gCOURSES_COLS[gCOURSES_DURATION_COL] },
        { data: gCOURSES_COLS[gCOURSES_LEVEL_COL] },
        { data: gCOURSES_COLS[gCOURSES_COVERIMAGE_COL] },
        { data: gCOURSES_COLS[gCOURSES_TEACHERNAME_COL] },
        { data: gCOURSES_COLS[gCOURSES_TEACHERPHOTO_COL] },
        { data: gCOURSES_COLS[gCOURSES_ISPOPULAR_COL] },
        { data: gCOURSES_COLS[gCOURSES_TRENDING_COL] },
        { data: gCOURSES_COLS[gCOURSES_ACTION_COL] }
    ],
    columnDefs: [
        { // định nghĩa lại cột action
            targets: gCOURSES_ACTION_COL,
            defaultContent: `
                <button class='btn btn-info btn-update-course'> Sửa </button> 
                <button class='btn btn-danger btn-delete-course'> Xoá </button>
            `
        }
    ]
});
$(document).ready(function(){
    loadDataToUserTable(gCoursesDB.courses)
    $("#btn-add-course").on("click", function() {
        onBtnAddNewCourseClick();
    });
    $("#btn-create-course").on("click", function() {
        onBtnCreateCourseClick();
    });
    $("#courses-table").on("click", ".btn-update-course", function() {
      onBtnUpdateCourseClick(this);
    });
    $("#btn-update-course-modal").on("click", function() {
      onBtnUpdateUpdateModalClick();
    });
    $("#courses-table").on("click", ".btn-delete-course", function() {
      onBtnDeleteCourseClick(this);
    });
    $("#btn-confirm-delete-voucher").on("click", function() {
      onBtnConfirmCourseClick();
    });
})
function onBtnConfirmCourseClick() {  
  // khai báo đối tượng chứa voucher data
  // B1: Thu thập dữ liệu
  // B2: Validate update
  // B3: update voucher
  for(var bIndex in gCoursesDB.courses){
    if(gCoursesDB.courses[bIndex].id == gCourse.id){
      gCoursesDB.courses.splice(bIndex, 1)
    }
  }
  $("#delete-confirm-modal").modal("hide");
  loadDataToUserTable(gCoursesDB.courses)
  
}
function onBtnDeleteCourseClick(paramBtnDelete) {
  // lưu thông tin UserId đang được edit vào biến toàn cục
  gCourse = getCourseFromButton(paramBtnDelete);
  $("#delete-confirm-modal").modal("show");
}
function onBtnUpdateUpdateModalClick() {
    var vCourseObject = {
      id: 0,
      courseCode: "",
      courseName: "",
      price: 0,
      discountPrice: 0,
      duration: "",
      level: "",
      coverImage: "",
      teacherName: "",
      teacherPhoto: "",
      isPopular: false,
      isTrending: false,
    };
    // B1: Thu thập dữ liệu
    getUpdateCourseData(vCourseObject);
    // B2: Validate insert
    if (validateCourseData(vCourseObject)) {
      for(var bIndex in gCoursesDB.courses){
        if(gCoursesDB.courses[bIndex].id == vCourseObject.id){
          gCoursesDB.courses[bIndex] = vCourseObject
        }
      }
      $("#update-course-modal").modal("hide")
      loadDataToUserTable(gCoursesDB.courses)
    }
}
function getUpdateCourseData(paramObjectUpdate){
    paramObjectUpdate.id = $('#update-course-modal #inp-course-id').val().trim()
    paramObjectUpdate.courseCode = $('#update-course-modal #inp-course-code').val().trim()
    paramObjectUpdate.courseName = $('#update-course-modal #inp-course-name').val().trim()
    paramObjectUpdate.price = Number($('#update-course-modal #inp-course-price').val().trim())
    paramObjectUpdate.discountPrice = Number($('#update-course-modal #inp-course-discountprice').val().trim())
    paramObjectUpdate.duration = $('#update-course-modal #inp-course-duration').val().trim()
    paramObjectUpdate.level = $('#update-course-modal #inp-course-level').val().trim()
    paramObjectUpdate.coverImage  =$('#update-course-modal #inp-course-coverimage').val().trim()
    paramObjectUpdate.teacherName = $('#update-course-modal #inp-course-teachername').val().trim()
    paramObjectUpdate.teacherPhoto = $('#update-course-modal #inp-course-teacherphoto').val().trim()
    paramObjectUpdate.isPopular = $('#update-course-modal #checkbox-popular').is(':checked');
    paramObjectUpdate.isTrending = $('#update-course-modal #checkbox-trending').is(':checked');
}
function getCourseFromButton(paramButton) {
  var vTableRow = $(paramButton).parents("tr");
  var vCourseRowData = gCoursesTable.row(vTableRow).data();
  return vCourseRowData;
}
function loadDataToUserTable(paramCoursesArr) {
    gCoursesTable.clear();
    gCoursesTable.rows.add(paramCoursesArr);
    gCoursesTable.draw();
}
//bấm nút thêm course thì hiển thị modal
function onBtnAddNewCourseClick() {
    // hiển thị modal trắng lên
    $("#create-course-modal").modal("show");
}
function onBtnUpdateCourseClick(paramButtonUpdate){
  gCourse = getCourseFromButton(paramButtonUpdate)
  $("#update-course-modal").modal("show")
  $('#update-course-modal #inp-course-id').val(gCourse.id)
  $('#update-course-modal #inp-course-code').val(gCourse.courseCode)
  $('#update-course-modal #inp-course-name').val(gCourse.courseName)
  $('#update-course-modal #inp-course-price').val(gCourse.price)
  $('#update-course-modal #inp-course-discountprice').val(gCourse.discountPrice)
  $('#update-course-modal #inp-course-duration').val(gCourse.duration)
  $('#update-course-modal #inp-course-level').val(gCourse.level)
  $('#update-course-modal #inp-course-coverimage').val(gCourse.coverImage)
  $('#update-course-modal #inp-course-teachername').val(gCourse.teacherName)
  $('#update-course-modal #inp-course-teacherphoto').val(gCourse.teacherPhoto)
  if(gCourse.isPopular == true){
    $('#update-course-modal #checkbox-popular').prop('checked', true)
  }else{
    $('#update-course-modal #checkbox-popular').prop('checked', false)
  }
  if(gCourse.isTrending == true){
    $('#update-course-modal #checkbox-trending').prop('checked', true)
  }else{
    $('#update-course-modal #checkbox-trending').prop('checked', false)
  }
}
function onBtnCreateCourseClick(){
    var vCourseObject = {
        id: 0,
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: false,
        isTrending: false,

    };
    // B1: Thu thập dữ liệu
    getCreateCourseData(vCourseObject);
    // B2: Validate insert
    if(validateCourseData(vCourseObject)) {
      gCoursesDB.courses.push(vCourseObject)
      $("#create-course-modal").modal("hide")
      removeDataElementForm()
      loadDataToUserTable(gCoursesDB.courses)
    }
}
// lấy dữ liệu từ form
function getCreateCourseData(paramCourseObject) {
    paramCourseObject.id = getNextId();
    paramCourseObject.courseCode = $('#inp-course-code').val().trim()
    paramCourseObject.courseName = $('#inp-course-name').val().trim()
    paramCourseObject.price = Number($('#inp-course-price').val().trim())
    paramCourseObject.discountPrice = Number($('#inp-course-discountprice').val().trim())
    paramCourseObject.duration = $('#inp-course-duration').val().trim()
    paramCourseObject.level = $('#inp-course-level').val().trim()
    paramCourseObject.coverImage  =$('#inp-course-coverimage').val().trim()
    paramCourseObject.teacherName = $('#inp-course-teachername').val().trim()
    paramCourseObject.teacherPhoto = $('#inp-course-teacherphoto').val().trim()
    paramCourseObject.isPopular = $('#checkbox-popular').is(':checked');
    paramCourseObject.isTrending = $('#checkbox-trending').is(':checked');
}
function removeDataElementForm(){
  $('#inp-course-code').val('')
  $('#inp-course-name').val('')
  $('#inp-course-price').val('')
  $('#inp-course-discountprice').val('')
  $('#inp-course-duration').val('')
  $('#inp-course-level').val('')
  $('#inp-course-coverimage').val('')
  $('#inp-course-teachername').val('')
  $('#inp-course-teacherphoto').val('')
  $('#checkbox-popular').prop('checked', false)
  $('#checkbox-trending').prop('checked', false)
}
// validate dũ liệu form
function validateCourseData(paramCourseObject){
    if(paramCourseObject.courseCode == ""){
      return false;
    }
    if(paramCourseObject.courseName == ""){
      return false;
    }
    if(paramCourseObject.price == ""){
      return false;
    }
    if(paramCourseObject.duration == ""){
      return false;
    }
    if(paramCourseObject.level == ""){
      return false;
    }
    if(paramCourseObject.coverImage == ""){
      return false;
    }
    if(paramCourseObject.teacherName == ""){
      return false;
    }
    if(paramCourseObject.teacherPhoto == ""){
      return false;
    }
    return true;
}
// hàm sinh ra đc id tự tăng tiếp theo, dùng khi Thêm mới phần tử
function getNextId() {
    var vNextId = 0;
    // Nếu mảng chưa có đối tượng nào thì Id = 1
    if(gCoursesDB.courses.length == 0) {
      vNextId = 1;
    }
  // Id tiếp theo bằng Id của phần tử cuối cùng + thêm 1    
else {
      vNextId = gCoursesDB.courses[gCoursesDB.courses.length - 1].id + 1;
    }
    return vNextId;
  }

